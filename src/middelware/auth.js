const jwt = require('jsonwebtoken');
const User = require('../model/user')

const auth = async (req,res,next) => {
    if (req.headers['authorization']) {
        try {
            const authheader = req.headers.authorization
            const token = authheader.replace('Bearer ','')
            const decoded = await jwt.verify(token,'secretkey')
            const user = await User.findOne({_id:decoded._id,'tokens.token':token})
            if(!user){
                throw new Error('user not found for this token')
            }
            req.token = token
            req.user = user
            next()
        } catch (err) {
            return res.status(403).json({error:err.message});
        //    next(err)
        }
    } else {
        return res.status(401).json({error:'invalid request!'});
    }

}

module.exports = auth