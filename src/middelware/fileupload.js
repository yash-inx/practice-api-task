const util = require("util");
const multer = require("multer");
// const maxSize = 2 * 1024 * 1024;  //2mb
const path = require('path')

const storagePath = path.join(__dirname,'../public/assets/uploads')

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, storagePath);
    },
    filename: (req, file, cb) => {
      if(!file.originalname.match(/\.(zip)$/i)){
         return cb(new Error('please Enter a valid Zip File'));
      } 
      var ext = path.extname(file.originalname);
    
      cb(null, file.fieldname + '-' + Date.now()+ext)
     },
      limits: {
        fileSize: 1000000
    }
  });

let uploadFile = multer({storage: storage}).single("zipFile");

let uploadFileMiddleware = util.promisify(uploadFile);
module.exports = uploadFileMiddleware;