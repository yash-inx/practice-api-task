//express-validator
const {body} = require('express-validator')

exports.user_validator = [
    body('firstname')
        .not()
        .isEmpty()
        .withMessage('firstname is Required'),
    body('lastname')
        .not()
        .isEmpty()
        .withMessage('lastname is Required'),
    body('email')
        .not()
        .isEmpty()
        .withMessage('email Required')
        .isEmail()
        .withMessage('it should be an email'),
    body('password')
        .not()
        .isEmpty()
        .withMessage('Password Required')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/)
        .withMessage('password must be one character one number one special character')
]

exports.update_validator = [
    body('email')
        .isEmail()
        .withMessage('it should be an email'),
    // check('password').isLength({min:8}).withMessage('password must be atleast 8 character long')
]

exports.login_validator = [
    body('email')  
        .isEmail()
        .withMessage('it should be an email'),
    body('password')
        .not()
        .isEmpty()
        .withMessage('Password Required')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/)
        .withMessage('password must be one character one number one special character')
]


exports.changePass_validator = [
    body('oldPassword')
        .not()
        .isEmpty()
        .withMessage('oldPassword Required')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/)
        .withMessage('oldPassword must be one character one number one special character'),
    body('newPassword')
        .not()
        .isEmpty()
        .withMessage('newPassword Required')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/)
        .withMessage('newPassword must be one character one number one special character')
]

exports.forgotPass_validator = [
    body('email')
        .isEmail()
        .withMessage('it should be an email')
]

exports.resetPass_validator = [
    body('password')
        .not()
        .isEmpty()
        .withMessage('newPassword Required')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/)
        .withMessage('password must be one character one number one special character'),
]