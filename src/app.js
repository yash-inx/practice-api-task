var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

require('dotenv').config()

var userRouter = require('./routes/user');

const db = require('./config/database')

// const expressValidator = require('express-validator')
 
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//Database connection with mongodb
const mongoose = require('./config/database');
// app.use(expressValidator())



app.use('/user', userRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
 
  if (err.name === 'ValidationError') {
    // mongoose validation error
    return res.status(400).json({ error: err.message });
  }

  if (err.name === 'UnauthorizedError') {
    // jwt authentication error
    return res.status(401).json({ error: 'Invalid Token' });
  }
  if(err.message === 'Authentication failed')
  {
      //invalid password  
      return res.status(401).json({error:err.message})
  }
  
  res.status(err.status || 500);
  res.json({error:res.locals.message});
});

module.exports = app;
