const path = require('path')
const fs = require('fs')
const fsExtra = require('fs-extra')
const User = require('../model/user')

const jwt = require('jsonwebtoken')

const { transport } = require('../services/email.service')

const uploadFile = require("../middelware/fileupload");

const { extractDataWithAdmZip, compressData } = require('../services/compress.service')

const { getListOfFilesOrFolders } = require('../helper/fileSystem.helper')

const { removeFile } = require('../helper/commonFunction.helper')

exports.register = async (req,res,next)=>{
    const user = new User(req.body)
    try {

        const existed = await User.findOne({email:user.email})
       
        if(existed){
            throw new Error(`Username "${user.email }" already taken`)
        }
        const token = await user.generateAuthtoken()
        await user.save()
        return res.status(201).json({message:"Registered Successfully!!",result:user})
    } catch (err) {
        next(err)
    }
}

exports.updateProfile = async (req,res,next) => {
    try {


        const updates = Object.keys(req.body)
        const allowedUpdates = ['firstname','lastname','email','password']
        const isvalidOperation = updates.every((update) => allowedUpdates.includes(update))
        
        if(!isvalidOperation){
            return res.status(400).json({ error: 'Invalid updates!' })
        }

        updates.forEach((update) => req.user[update] = req.body[update])
        await req.user.save()

        return res.status(200).json({message:"updated Successfully!!",result:req.user})
    } catch (err) {
        next(err)   
    }
}

exports.viewProfile = async (req,res,next) => {
    try {
        if (req.user) {
            return res.status(200).json({ message: 'fetched Successfully',result:req.user });
        } 
    } catch (err) {
        next(err)   
    }
}

exports.login = async (req,res,next)=>{
    try {


        const user = await User.findOne({email:req.body.email})
        
        if(!user){
            return res.status(404).json({error:"User with this Email is not Found"})
        }
        
        const token = await user.generateAuthtoken()
        
        await user.comparePassword(req.body.password)
        
        return res.status(200).json({message:"Successfully logged In!!",result:user})
       
    } catch (err) {
        next(err)
    }
}

exports.logOut = async (req,res,next) => {
    try {        
        req.user.tokens = []
        await req.user.save()
        return res.status(200).json({ message: 'Logout Successfully',result:req.user });
    } catch (err) {
        next(err)
    }
}

exports.editPassword = async (req,res,next) => {
    try {


        const { oldPassword,newPassword } = req.body
        
        await req.user.comparePassword(oldPassword)

        req.user.password = newPassword
        await req.user.save()
        return res.status(200).json({ message: 'Change Password Successfully.',result:req.user });
        
    } catch (err) {
        next(err)
    }
}

exports.forgotPassword = async (req,res,next) => {
    try {

        const { email } = req.body
        const user = await User.findOne({email})
        if(!user){
            return res.status(400).json({error: 'User with this email address does not exists'})
        }
        
        const token = jwt.sign({_id:user._id.toString()},'resetpasswordsecretkey',{expiresIn:'20m'})
        
        await user.updateOne({resetLink:token})
        await user.save()
        
        //create email
        const message = {
            from: process.env.EMAIL_USER,
            to: email,
            subject: 'Forgot Password Link...',
            text:`To reset your password, please click the link below.\n\n http://localhost:3000/user/reset-password/${token}/${email}`
        }

    
        //  });
        
        const sent = await transport.sendMail(message)
        if(sent){
            return res.status(200).json({message:"Email sent...Please Check your email and follow some instruction"})
        }
        
    } catch (err) {
        next(err)
    }
}

exports.resetPassword = async (req,res,next) => {
    try {
        const {token, email} = req.params
        const user = await User.findOne({resetLink:token})
        if(!user){
            return res.status(400).json({error: 'Reset Link Expired...Please try the reset password process again.'})
        }
        const check = await jwt.verify(token,'resetpasswordsecretkey')
        return res.render('reset-password',{email:email})
    } catch (err) {
        next(err)
    }
}

exports.changePassword = async (req,res,next) => {
    try {
       
        const {token, email} = req.params
        const {password,password2} = req.body
        const user = await User.findOne({resetLink:token})
        if(!user){
            return res.status(400).json({error: 'Reset Link Expired...Please try the reset password process again.'})
        }

        const check = await jwt.verify(token,'resetpasswordsecretkey')

        if(password !== password2){
            return res.status(401).json({error: 'Passwords do not match. Please try again.'});
        }
        user.password = password2
        user.resetLink = ''
        await user.save()
        return res.status(200).json({message: 'Congrats..Your password is change'})
    } catch (err) {
        next(err)
    }
}

exports.upload = async (req,res,next) => {
    try {
        await uploadFile(req,res)
        const filePath = req.file.path
        if (req.file == undefined) {
            return res.status(400).json({ error: "Please upload a file!" });
        }

        destPath = path.join(filePath,'../extracted')
    
        if (!fs.existsSync(destPath)) {
            fs.mkdirSync(destPath, {
                recursive: true
            });
        }
      
        await fsExtra.emptyDirSync(destPath)

        await extractDataWithAdmZip(filePath,destPath)

        let extratedFileList = await getListOfFilesOrFolders(destPath,'FILE')
        console.log("Extracted >>>>>>>>>>>>> " + extratedFileList);

        if(!extratedFileList.includes('index.html')){
            console.log("index.html file not found");
            // return res.status(404).json({ message: "Index.html file not found" });
        }else{
            console.log("indext.html file Exists");
        }

        
        
        // backupPath = path.join(filePath,'../../backup')
        // compressFileStatus = await compressData(destPath,backupPath)
        // console.log("compression "+compressFileStatus);

        // if(compressFileStatus == null){
        //     return res.status(400).json({error:'error occured in backing up'})
        // }
       
        await removeFile(filePath);
        
        return res.status(200).json({
            message: "Uploaded the file successfully: " + req.file.originalname,
        });
    } catch (err) {
        if (err.code == "LIMIT_FILE_SIZE") {
            return res.status(500).json({
              message: "File size cannot be larger than 2MB!",
            });
        }
        next(err)
    }
}

// exports.getUsers = async (req,res,next) => {
//     try {
//         if (req.user) {
//            const user = await User.find()
//            return res.status(200).json({ message: 'fetched Successfully',result:user });
//         } else {
//             return res.status(401).json({ message: 'Unauthorized user!!' });
//         }
//     } catch (err) {
//         next(err)
//     }
// }
