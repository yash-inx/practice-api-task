const fs = require('fs')
const {validationResult} = require('express-validator')

// show validation error message
exports.validatorFunc = (req, res, next) => {
    let errArray = {};
    const errors = validationResult(req);
    console.log("errors.....",errors)
    if (!errors.isEmpty()) {
        return res.status(422).jsonp(errors.array());
    }
    next();
};

exports.removeFile = function (delPath) {
    if(fs.existsSync(delPath)){
        fs.unlinkSync(delPath)
    }
}