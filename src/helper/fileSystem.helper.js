const fs = require('fs')

let getListOfFilesOrFolders = async (directoryPath, FS_TYPE) => {
    let filesArray = []
    let filesList =  fs.readdirSync(directoryPath)
    

    for(let i=0; i < filesList.length; i++){
        let fileNames = await new Promise((resolve, reject) => {
            fs.stat(directoryPath + `/${filesList[i]}`, function(err, stats) {
                
                if(FS_TYPE == 'DIR' && stats.isDirectory() == true){
                    resolve(filesList[i]);
                }else if(FS_TYPE == 'FILE' && stats.isFile() == true){
                    resolve(filesList[i]);
                }else{
                    resolve(null);
                }
            })
        })

        if(fileNames){
            filesArray.push(fileNames);
        }
    }
    
    return filesArray;
   
}

module.exports = { getListOfFilesOrFolders }