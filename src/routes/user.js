const express = require('express')
const router = express.Router()

//controller
const { 
    register,
    login,
    logOut,
    forgotPassword,
    updateProfile,
    viewProfile,
    resetPassword,
    changePassword,
    editPassword,
    upload } = require('../controller/user.controller')


//Validators
const { 
    user_validator,
    update_validator,
    login_validator,
    changePass_validator,
    forgotPass_validator,
    resetPass_validator } = require('../validation/user.middelware')



//helper
const { validatorFunc } = require('../helper/commonFunction.helper')

//middelware
const auth = require('../middelware/auth')




//endpoints

//register
router.post('/register',user_validator,validatorFunc, register)

//first you have to login
//update Profile
router.patch('/update',update_validator,validatorFunc,auth,updateProfile)

//View Profile 
router.get('/view',auth,viewProfile)


//login
router.post('/login',login_validator,validatorFunc,login)

//logout
router.post('/logout',auth, logOut)

router.patch('/change-password',changePass_validator,validatorFunc,auth,editPassword)
    
// router.get('/getUsers',auth,userController.getUsers)

//forgot password with email
router.post('/forgot-password',forgotPass_validator,validatorFunc,forgotPassword)

router.get('/reset-password/:token/:email',resetPassword)

router.post('/reset-password/:token/:email',resetPass_validator,validatorFunc,changePassword)


router.post('/upload',auth,upload)    

module.exports = router