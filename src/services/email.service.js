const nodemailer = require('nodemailer')


let transport = nodemailer.createTransport({
    host: 'smtp.outlook.com',
    port: 587,
    secure: false,
    service: 'outlook',
    auth: {
       user: process.env.EMAIL_USER,
       pass: process.env.EMAIL_PASS
    }
});

module.exports = { transport } 