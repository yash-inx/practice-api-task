const fs = require('fs')

const fsHelper = require('../helper/fileSystem.helper')
const AdmZip = require('adm-zip')
const archiver = require('archiver');

let extractDataWithAdmZip = async (sourcePath,destinationPath) => {
    let zip = new AdmZip(sourcePath)
    let zipEntries = zip.getEntries();
    zip.extractAllTo(destinationPath, true);
}

let compressData = async (sourceDirPath, zipDestinationPath) => {
    let compressionResult = []
    
    let fileList = await fsHelper.getListOfFilesOrFolders(sourceDirPath,'FILE')
    let dirList =  await fsHelper.getListOfFilesOrFolders(sourceDirPath,'DIR')
    
    if(fileList.length > 0 || dirList.length > 0){
        compressionResult = await new Promise((resolve,reject)=>{
            let output = fs.createWriteStream(zipDestinationPath)
            let archive = archiver('zip')

            output.on('close', function() {
                resolve(true);
            });
            archive.on('error',function(err){
                resolve(null)
            })
            archive.pipe(output)
            
            for(let i = 0; i < fileList.length; i++){
                var path = sourceDirPath + '/' + fileList[i]
                archive.append(fs.readFileSync(path),{name : fileList[i]})
            }

            for(let d = 0; d < dirList.length; d++){
                var path = sourceDirPath + '/' + dirList[d]
                archive.directory(path, dirList[d])
            }

            archive.finalize();
        })
    }
    
    return compressionResult;
}

module.exports = { extractDataWithAdmZip, compressData }