/* mongoose configuration*/
const mongoose = require('mongoose');


//database configuration

console.log('MONGODB_URI.........',process.env.MONGO_URI,)

mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true,useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true});

mongoose.connection.on('connected', function(){
    console.log('Database Connection Established.');
});

mongoose.connection.on('error', function(err){
    console.log('Mongodb connection failed. '+err);
    mongoose.disconnect();
});

mongoose.connection.once('open', function() {
	console.log('MongoDB connection opened!');
});

mongoose.connection.on('reconnected', function () {
	console.log('MongoDB reconnected!');
});

mongoose.connection.on('disconnected', function() {
	console.log('MongoDB disconnected!');
	mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true,useCreateIndex: true, useFindAndModify: false, useUnifiedTopology: true},/*{server:{auto_reconnect:true}}*/);
});
	
module.export = mongoose;
