const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    
    firstname:{
        type:String,
        required:true
    },
    lastname:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        minlength:8,
        required:true
    },
    tokens:[{
        token:{
            type:String,
            required:true
        }
    }],
    resetLink:{
        data:String,
        default:''
    }
},{timestamps:true})

userSchema.pre('save', async function(next) {
    try {
        if(!this.isModified('password')){
            return next()
        }
        const salt = await bcrypt.genSalt(10)
        const hashedPassword = await bcrypt.hash(this.password,salt)
        this.password = hashedPassword
         next()
    } catch (err) {
        next(err)
    }
   
})

userSchema.methods.comparePassword = async function(password,next){
    const isMatch = await bcrypt.compare(password,this.password)
    if(!isMatch){
       throw new Error('Authentication failed')
    }
    return this
}

userSchema.methods.generateAuthtoken = async function(){
    const token = jwt.sign({_id:this._id.toString()},'secretkey',{expiresIn:'5h'})
    this.tokens = this.tokens.concat({token})
    await this.save()
    return token
}


const User = new mongoose.model('User',userSchema)

module.exports = User